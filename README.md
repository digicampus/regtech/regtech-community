# RegTech Community

In deze community verzamelen en bediscussiëren wij de nieuwste ontwikkelingen op het gebied van Regulatory Tech: 
Technologie die te maken heeft met wetten, regels en normen.

Draag bij op [de wiki](https://gitlab.com/digicampus/regtech/regtech-community/-/wikis/home)

## License

The content in this repository and the wiki is licensed under Creative Commons v4, Attribution-ShareAlike. See the [LICENSE file](./LICENSE) or the [online summary](https://creativecommons.org/licenses/by-sa/4.0/) for details.
